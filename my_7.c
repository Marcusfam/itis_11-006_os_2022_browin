#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/dir.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

int file_counter = 0;

int iter_directory(char *path, int min, int max, FILE *file1) {
	DIR *dir1;
	struct stat buf;
	struct dirent *dir;
	int check_open = 0;

	if ((dir1=opendir(path))==NULL) {
		printf("Error: OpenDirError.\n");
		return -1;
	} else {
		dir = readdir(dir1);
		while(dir!=NULL){
			char local_path[(strlen(path)) + (strlen(dir->d_name)) + 1];
			sprintf(local_path, "%s%s%s", path, "/", dir->d_name);
			lstat(local_path, &buf);

			if (S_ISDIR(buf.st_mode)) {
				if ((strcmp(dir->d_name, "..")) && (strcmp(dir->d_name, "."))) {
					check_open = iter_directory(local_path, min, max, file1);
				}
			}else if(S_ISREG(buf.st_mode)) {
				if((buf.st_size < max) && (buf.st_size > min)) {
					char file_string[strlen(local_path)) + (strlen(dir->d_name)) + ((sizeof (buf.st_size))/(sizeof(int))) + 7];
					sprintf(file_string, "%s%s%s%s%ld%s", local_path, " | ", dir->d_name, " | ", buf.st_size, "\n");

				    if(fputs(file_string, file1)==0){
				    	printf("Error: WriteFileError\n");
				    	return -1;
				    }
				}
				file_counter++;
			} else {
				printf("Error: ReadFileError.\n");
				return -1;
			}
			dir = readdir(dir1);
		}
	}
	if(closedir(dir1)!=0){
		printf ("DirCloseError.\n");
		return -1;
	}
	return check_open;
}
 
int main(int argc, char *argv[]) {
	FILE *file1;
	int result;
	int min = atoi(argv[2]);
	int max = atoi(argv[3]);
	char *path;
	realpath(argv[1], path);

	if ((file1=fopen(argv[4], "w"))==NULL) {
		printf("Error: OpenFileError.\n");
		return -1;
	}
	result = iter_directory(path, min, max, file1);

	if ((fclose(file1))!=0) {
		printf("Error: CloseFileError.\n");
		return -1;
	}
	printf("Files checked: %d\t\n", file_counter);
	return result;
}
